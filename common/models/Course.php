<?php
namespace common\models;
use \yii\db\ActiveRecord;
/**
 * Course Model
 *
 * @author Maxim Polosin <polosinms@mail.ru>
 */
class Course extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'courses';
	}
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['id'];
    }
    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required']
        ];
    }
}