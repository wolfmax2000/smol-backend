<?php
namespace api\controllers;
use yii\rest\ActiveController;
/**
 * Courses Controller API
 *
 * @author Maxim Polosin <polosinms@mail.ru>
 */
class CourseController extends ActiveController
{
    public $modelClass = 'common\models\Course';
}